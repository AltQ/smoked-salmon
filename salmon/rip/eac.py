import glob
import os
from base64 import standard_b64encode
from dataclasses import dataclass
from hashlib import sha1
from time import sleep

from cueparser import CueSheet
from pywinauto.application import Application

@dataclass
class Disc:
    toc: list[int]
    cue: CueSheet | None = None

    @property
    def TOCID(self):
        tocid = ''
        if self.toc:
            tocid.join(f'{offset:08X}' for offset in self.toc[1:])
        tocid = tocid.ljust(800, '0')
        tocid = standard_b64encode(sha1(tocid.encode('utf-8')).digest()).decode('utf-8')
        return tocid.replace('+', '.').replace('/', '_').replace('=', '-')

    def metadata_from_cue(self, cue_path):
        cue = CueSheet()
        with open(cue_path) as f:
            cue.data = f.read()

        cue.parse()


# print(generate_tocid(track_offsets))
# @dataclass
# class Album:
#     title: str
#     artist: str
#     year: int
#     performer: str
#     genre: str
#     tracks: list[Track]
#     various_artists: bool

# @dataclass
# class Track:
#     length: int  # in frames


class EAC:
    ripdir = 'C:/rip'

    def __init__(self) -> None:
        self.app = Application(backend="win32").start(
            'C:/Program Files (x86)/Exact Audio Copy/EAC.exe'
        )
        self.define_window_specs()
        # self.app.

        self.main_window_spec.exists(10)
        sleep(1)
        self.menu = self.main_window_spec.menu()
        self.current_disc = None
        if self.cd_in_tray:

            self.read_from_cd_text()
            log_path = self.make_aborted_log()
            self.current_disc = Disc(self.get_TOC(log_path))

    @property
    def cd_in_tray(self):
        return self.main_window_spec.child_window(title='Audio CD in drive').exists()

    def define_window_specs(self):
        self.main_window_spec = self.app.window(class_name='erstes')
        self.rip_dialog_spec = self.app.window(
            class_name='#32770', title='Extracting Audio Data'
        )
        self.rip_cancel_warning_spec = self.app.window(
            class_name='#32770',
            title='Warning',
            predicate_func=lambda win: 'Do you really want to cancel extraction ?'
            in (child.name for child in win.children()),
        )
        self.file_overwrite_warning_spec = self.app.window(
            class_name='#32770',
            title='Warning',
            predicate_func=lambda win: 'File already exists !'
            in (child.name for child in win.children()),
        )
        self.analyzing_dialog_spec = self.app.window(
            class_name='#32770', title='Analyzing'
        )

    def get_TOC(self, log_path: str) -> list[int]:

        aborted_log_path = self.make_aborted_log()
        with open(aborted_log_path, 'r') as f:
            line = ''

            def get_line(n: int):
                return [f.readline().replace('\x00', '') for _ in range(n)][-1]

            # skip to start of disc TOC
            while not '--------------------------------------------------' in (
                line := get_line(1)
            ):
                pass
                # line = get_line(1)

            line = get_line(2)
            toc = []
            while len(timestamps := line.split('|')) == 5:

                start_sector = int(timestamps[3].strip(' '))
                end_sector = int(timestamps[4].strip(' '))
                toc.append(int(start_sector))

                line = get_line(2)

            self.current_disc = toc.append(end_sector + 1)

    def make_aborted_log(self):
        self.menu.item('Action -> Copy Selected Tracks -> Compressed').click()
        if self.file_overwrite_warning_spec.exists(5):
            self.file_overwrite_warning_spec['No to all'].click()
            self.rip_dialog_spec.child_window(title='Cancel').wait_not('exists', 1)

        if self.rip_dialog_spec.child_window(title='Cancel').exists(1):
            self.rip_dialog_spec.child_window(title='Cancel').click()

            self.rip_cancel_warning_spec.exists(10)
            self.rip_cancel_warning_spec.Yes.click()

        # print(self.rip_dialog_spec.dump_tree())
        if self.rip_dialog_spec.child_window(title='OK').exists(10):
            self.rip_dialog_spec.child_window(title='OK').click()

        list_of_logs = glob.glob(
            f'{self.ripdir}/*/*.log'
        )  # * means all if need specific format then *.csv
        return max(list_of_logs, key=os.path.getctime)

    def read_from_cd_text(self):
        self.menu.item('Database -> Get Information From -> CD-TEXT').click()

    def test_and_copy(self):
        self.menu.item('Action -> Test & Copy Selected Tracks -> Compressed').click()

    def make_cue(self):
        if not self.gaps_detected:
            self.detect_gaps()

        self.menu.item(
            'Action -> Create CUE Sheet -> Multiple WAV Files with Gaps... (Noncompliant)'
        ).click()
        if not self.cue_made:
            self.analyzing_dialog_spec.wait('exists')  # for detecting isrc
            self.analyzing_dialog_spec.wait_not('exists')
            self.cue_made = True

        list_of_cues = glob.glob(
            f'{self.ripdir}/*/*.cue'
        )  # * means all if need specific format then *.csv
        self.current_disc.metadata_from_cue(max(list_of_cues, key=os.path.getctime))
        return self.current_disc.cue

    def detect_gaps(self):
        self.menu.item('Action -> Detect Gaps').click()
        if not self.gaps_detected:  # i.e. if this isn't an extra detection
            self.analyzing_dialog_spec.wait('exists')
            self.analyzing_dialog_spec.wait_not('exists')
            self.gaps_detected = True

    def read_from_cd_text(self):
        self.menu.item('Database -> Get Information From -> CD-TEXT').click()

    def eject_cd(self):
        self.main_window_spec['Eject CD'].wrapper_object().click()
        self.cue_made = False
        self.gaps_detected = False

    def fix_latest_cue(self):
        list_of_cues = glob.glob(
            f'{self.ripdir}/*.cue'
        )  # * means all if need specific format then *.csv
        latest_cuefile = max(list_of_cues, key=os.path.getctime)
        cue = CueSheet()

        with open(latest_cuefile, "rb") as f:
            data = f.read().decode('latin-1')
            cue.setData(data)

            cue.parse()
            dir_name = cue.file[0].split('\\')[0]

            with open(f'{dir_name}/{latest_cuefile}', 'w') as f2:
                f2.write(data.replace(f'{dir_name}\\', '').replace('.wav', '.flac'))

        print(cue)

    def rip_cd(self):

        if not self.gaps_detected:
            self.detect_gaps()
        if not self.current_disc.cue_made:
            self.make_cue()

        self.test_and_copy()

        self.eject_cd()

    def autorip(self, tocid_checking_callback):
        while not self.cd_in_tray:
            sleep(1)

        self.read_from_cd_text()

        toc = self.get_TOC()
        tocid = self.current_disc.TOCID
        cue = self.make_cue()
        artists = cue.performer.split('&')
        album = cue.title


# def search_CTDB(TOC):

a = EAC()


print((a.current_disc.TOCID))
