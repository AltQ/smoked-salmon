import click
import requests
from requests.exceptions import ConnectTimeout, ReadTimeout

from salmon import config
from salmon.trackers.base import GazelleApi


class OpsApi(GazelleApi):
    site_code = 'OPS'
    base_url = 'https://orpheus.network'
    tracker_url = 'https://home.opsfet.ch'
    site_string = 'OPS'
    
    def __init__(self):
        super(self).__init__()
        self.headers = {
            "Connection": "keep-alive",
            "Cache-Control": "max-age=0",
            "User-Agent": config.USER_AGENT,
        }
        
        if config.OPS_DOTTORRENTS_DIR:
            self.dot_torrents_dir = config.OPS_DOTTORRENTS_DIR

        self.cookie = config.OPS_SESSION
