import asyncio
from typing import Self

import click
import requests
from requests.exceptions import ConnectTimeout, ReadTimeout

from salmon import config
from salmon.errors import (
    LoginError,
    RateLimitError,
    RequestError,
    RequestFailedError,
)
from salmon.trackers.base import GazelleApi

loop = asyncio.get_event_loop()


class RedApi(GazelleApi):
    site_code = 'RED'
    base_url = 'https://redacted.ch'
    tracker_url = 'https://flacsfor.me'
    site_string = 'RED'
    
    def __init__(self):
        super(self).__init__()
        
        if config.RED_API_KEY:
            self.api_key = config.RED_API_KEY
        if config.RED_DOTTORRENTS_DIR:
            self.dot_torrents_dir = config.RED_DOTTORRENTS_DIR

        self.cookie = config.OPS_SESSION

    async def report_lossy_master(self, torrent_id, comment, source):
        """Automagically report a torrent for lossy master/web approval.
        Use LWA if the torrent is web, otherwise LMA."""

        url = self.base_url + "/reportsv2.php"
        params = {"action": "takereport"}
        type_ = "lossywebapproval" if source == "WEB" else "lossyapproval"
        data = {
            "auth": self.authkey,
            "torrentid": torrent_id,
            "categoryid": 1,
            "type": type_,
            "extra": comment,
            "submit": True,
        }
        r = await loop.run_in_executor(
            None,
            lambda: self.session.post(
                url, params=params, data=data, headers=self.headers
            ),
        )
        if "torrents.php" in r.url:
            return True
        raise RequestError(
            f"Failed to report the torrent for lossy master, code {r.status_code}."
        )
